using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;

using Newtonsoft.Json;

namespace RedTail.Http
{
  public class HttpRequest
  {
    public Object Data { get; set; }
    public List<HttpHeader> Headers { get; private set; }
    public HttpMethod Method { get; set; }
    public String Url { get; set; }

    public void AddHeader(HttpHeader header)
    {
      this.Headers.Add(header);
    }

    public String Send()
    {
      Task<String> task = Task.Run(async () => await this.SendAsync());

      return task.Result;
    }

    private async Task<String> SendAsync()
    {
      using (HttpClient client = new HttpClient())
			{
				client.Timeout = new TimeSpan(0, 3, 0);

				if (this.Headers != null && this.Headers.Count > 0)
				{
					foreach (HttpHeader header in this.Headers)
					{
						client.DefaultRequestHeaders.Add(header.Key, header.Value);
					}
				}

				StringContent content = new StringContent("{}");
        if (this.Data != null && (this.Method == HttpMethod.Post || this.Method == HttpMethod.Put)) {
          content = new StringContent(JsonConvert.SerializeObject(this.Data), Encoding.UTF8, "application/json");
        }

				HttpResponseMessage httpResponseMessage;
        if (this.Method == HttpMethod.Delete) {
          httpResponseMessage = await client.DeleteAsync(this.Url);
        } else if (this.Method == HttpMethod.Get) {
          httpResponseMessage = await client.GetAsync(this.Url);
        } else if (this.Method == HttpMethod.Put) {
          httpResponseMessage = await client.PutAsync(this.Url, content);
        } else {
          httpResponseMessage = await client.PostAsync(this.Url, content);
        }

        String response = await httpResponseMessage.Content.ReadAsStringAsync();
				return response;
			}
    }
  }
}
