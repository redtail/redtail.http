namespace RedTail.Http
{
  public enum HttpMethod
  {
    Delete,
    Get,
    Post,
    Put
  }
}
