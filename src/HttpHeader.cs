using System;

namespace RedTail.Http
{
  public class HttpHeader
  {
    public String Key { get; set; }
    public String Value { get; set; }
  }
}
